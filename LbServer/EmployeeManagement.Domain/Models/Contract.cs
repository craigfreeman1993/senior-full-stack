﻿using System;

namespace EmployeeManagement.Domain
{
    public class Contract
    {
        private readonly DateTime _expirationDate;

        public Contract(DateTime expirationDate)
        {
            _expirationDate = expirationDate;
        }

        public bool IsExpired()
        {
            return _expirationDate <= DateTime.Now;
        }
    }
}
