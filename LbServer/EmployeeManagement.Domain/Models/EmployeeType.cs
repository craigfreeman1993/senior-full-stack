﻿namespace EmployeeManagement.Domain
{
    public enum EmployeeType
    {
        Director,
        Manager,
        Employee
    }
}
