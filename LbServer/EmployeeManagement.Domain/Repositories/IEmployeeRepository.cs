﻿using System.Collections.Generic;

namespace EmployeeManagement.Domain.Repositories
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> Get();
        Employee Get(int id);
        void Archive(int id);
    }
}
