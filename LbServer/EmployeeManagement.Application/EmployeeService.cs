﻿using EmployeeManagement.Domain;
using EmployeeManagement.Domain.Repositories;
using EmployeeManagement.Infrastructure;
using System.Collections.Generic;

namespace EmployeeManagement.Application
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository repo)
        {
            _employeeRepository = repo;
        }

        public void DeleteEmployee(int id)
        {
            _employeeRepository.Archive(id);
            EmployeeNotifier.NotifyEmployeeToLoginAndCheckTheirAccount(id);
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return _employeeRepository.Get();
        }
    }
}
