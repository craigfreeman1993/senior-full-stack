﻿using Microsoft.Extensions.DependencyInjection;

namespace EmployeeManagement.Application
{
    public static class EmployeeApplicationExtensions
    {
        public static IServiceCollection AddEmployeeApplication(this IServiceCollection services)
        {
            services.AddTransient<IEmployeeService, EmployeeService>();
            return services;
        }
    }
}
