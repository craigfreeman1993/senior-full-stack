﻿using EmployeeManagement.Domain.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace EmployeeManagement.Infrastructure
{
    public static class EmployeeInfrastructureExtensions
    {
        public static IServiceCollection AddEmployeeInfrastructure(this IServiceCollection services)
        {
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();

            return services;
        }
    }
}
