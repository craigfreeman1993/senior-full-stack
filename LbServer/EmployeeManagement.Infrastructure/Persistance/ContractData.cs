﻿using EmployeeManagement.Domain;
using System;

namespace EmployeeManagement.Infrastructure.Persistance
{
    public class ContractData
    {
        public int EmployeeId { get; set; }
        public DateTime ExpirationDate { get; set; }

        public Contract ToDomain()
        {
            return new Contract(ExpirationDate);
        }
    }
}
