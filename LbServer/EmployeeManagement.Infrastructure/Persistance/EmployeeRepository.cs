﻿using EmployeeManagement.Domain;
using EmployeeManagement.Domain.Repositories;
using EmployeeManagement.Infrastructure.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeManagement.Infrastructure
{
    internal class EmployeeRepository : IEmployeeRepository
    {
        public IEnumerable<Employee> Get()
        {
            return InMemoryStore.Employees.Select(e => {
                var contract = GetContract(e.Id)
                    .ToDomain();

                return e.ToDomain(contract);
            });
        }

        public Employee Get(int id)
        {
            var contract = GetContract(id)
                .ToDomain();

            return InMemoryStore.Employees
                .First(e => e.Id == id)
                .ToDomain(contract);
        }

        public void Archive(int id)
        {
            var employee = Get(id);

            if(!employee.CanBeArchived())
                throw new InvalidOperationException($"Cannot delete employee with id: {employee.Id}");

            var employeeData = InMemoryStore.Employees.First(e => e.Id == employee.Id);
            InMemoryStore.Employees.Remove(employeeData);

            var contract = GetContract(employee.Id);
            InMemoryStore.Contracts.Remove(contract);
        }

        private ContractData GetContract(int employeeId)
        {
            return InMemoryStore.Contracts
                    .First(c => c.EmployeeId == employeeId);
        }
    }
}
