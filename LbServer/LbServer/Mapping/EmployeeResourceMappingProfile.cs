﻿using Api.Resources;
using AutoMapper;
using EmployeeManagement.Domain;

namespace Api.Mapping
{
    public class EmployeeResourceMappingProfile : Profile
    {
        public EmployeeResourceMappingProfile()
        {
            CreateMap<Employee, EmployeeResource>()
                .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                .ForMember(d => d.Name, m => m.MapFrom(s => s.Name()));
        }
    }
}
