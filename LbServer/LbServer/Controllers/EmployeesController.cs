﻿using Api.Resources;
using AutoMapper;
using EmployeeManagement.Application;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace LbServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;

        public EmployeesController(IEmployeeService employeeService, IMapper mapper)
        {
            this._employeeService = employeeService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var employees = _employeeService.GetEmployees();
            var resources = _mapper.Map<IEnumerable<EmployeeResource>>(employees);
            return Ok(resources);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _employeeService.DeleteEmployee(id);
            return Ok();
        }
    }
}
