﻿namespace Api.Resources
{
    public static class EmployeeExtension
    {
        public static EmployeeResource ToResource(this EmployeeResource employee)
        {
            return new EmployeeResource
            {
                Id = employee.Id,
                Name = employee.Name
            };
        }
    }
}
