import { createReducer, on } from "@ngrx/store";
import { appActions } from "./app.actions";

export interface IAppState {
    isLoading: boolean;
}

export const initialState: IAppState = {
    isLoading: true
}

export const appReducer = createReducer(
    initialState,
    on(appActions.loading, (state, action) => ({
        ...state, isLoading: true
    })),
    on(appActions.finishedLoading, (state, action) => ({
        ...state, isLoading: false
    }))
);