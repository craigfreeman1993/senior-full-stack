import { Injectable } from "@angular/core";
import { createFeatureSelector, createSelector, select, Store } from "@ngrx/store";
import { IAppState } from "./app.reducers";

const getAppState = createFeatureSelector<IAppState>('app');
const getLoadingState = createSelector(getAppState, state => state.isLoading);

@Injectable({ providedIn: 'root' })
export class AppSelectors {
    constructor(private store: Store) {

    }

    isLoading$ = this.store
        .pipe(
            select(getLoadingState)
        );
}
