import { createAction, props } from "@ngrx/store";

export const appActions = {
    loading: createAction('[app] loader'),
    finishedLoading: createAction('[app] loader')
}