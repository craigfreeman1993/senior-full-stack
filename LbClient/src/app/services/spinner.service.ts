import { Injectable } from "@angular/core";
import { Observable, ReplaySubject } from "rxjs";
import { debounce, debounceTime, startWith } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class SpinnerService {
    private loadingSources = new Map<string, boolean>();

    private _loading = new ReplaySubject<boolean>(1);
    private loading$ = this._loading.pipe(startWith(false), debounceTime(100));

    SetIsLoading(identifier: string): void {
        this.loadingSources.set(identifier, true);
        this._loading.next(true);
    }

    SetHasFinishedLoading(identifier: string): void {
        this.loadingSources.set(identifier, false);

        if(this.anyLoadingSourcesAreFalse())
            this._loading.next(false);
    }

    IsLoading(): Observable<boolean> {
        return this.loading$;
    }

    private anyLoadingSourcesAreFalse(): boolean {
        return Array.from(this.loadingSources.values()).some(v => !v);
    }
}