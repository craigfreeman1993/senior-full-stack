import { Component } from '@angular/core';
import { AppSelectors } from 'src/app/store/app.selectors';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {
  isLoading$ = this.appSelectors.isLoading$;

  constructor(private appSelectors: AppSelectors) { }
}
