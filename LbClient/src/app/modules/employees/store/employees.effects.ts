import { Inject, Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { map, mergeMap, switchMap, tap } from "rxjs/operators";
import { appActions } from "src/app/store/app.actions";
import { EmployeeService } from "../services/employee.service";
import { EmployeeServiceToken, IEmployeeService } from "../services/IEmployeeService";
import { employeeActions } from "./employees.actions";

@Injectable()
export class EmployeesEffects {

    loadEmployees$ = createEffect(() => this.actions$.pipe(
        ofType(employeeActions.requestEmployees),
        tap(() => this.store.dispatch(appActions.loading())),
        mergeMap(() =>
            this.employeeService.getEmployees()
                .pipe(
                    switchMap(employees => [
                        employeeActions.requestEmployeesSuccess({ employees }),
                        appActions.finishedLoading()
                    ])
                )
        )
    ));

    deleteEmployee$ = createEffect(() => this.actions$.pipe(
        ofType(employeeActions.requestEmployeeDelete),
        tap(() => this.store.dispatch(appActions.loading())),
        mergeMap(action =>
            this.employeeService.delete(action.id)
                .pipe(
                    map(() => employeeActions.requestEmployeeDeleteSuccess({ id: action.id }))
                )
        )
    ));

    constructor(
        private store: Store,
        private actions$: Actions,
        @Inject(EmployeeServiceToken) private employeeService: IEmployeeService
    ) { }
}