import { Injectable } from "@angular/core";
import { createFeatureSelector, createSelector, select, Store } from "@ngrx/store";
import { IEmployeesState } from "./employees.reducer";

const selectEmployeesFeature = createFeatureSelector<IEmployeesState>('employees');

const selectEmployees = createSelector(
    selectEmployeesFeature,
    (state: IEmployeesState) => state.employees
);

@Injectable()
export class EmployeeSelectors {
    constructor(private store: Store) {

    }

    employees$ = this.store.pipe(select(selectEmployees));
}