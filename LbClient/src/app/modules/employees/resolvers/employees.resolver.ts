import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot, Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { employeeActions } from '../store/employees.actions';

@Injectable()
export class EmployeesResolver implements Resolve<void> {
  constructor(private store: Store) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): void {
    this.store.dispatch(employeeActions.requestEmployees());
  }
}
