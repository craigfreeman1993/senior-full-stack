import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';
import { IEmployee } from '../../resources/IEmployee';
import { employeeActions } from '../../store/employees.actions';
import { EmployeeSelectors } from '../../store/employees.selectors';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  employees: IEmployee[] = [];
  displayedColumns: string[] = ['name', 'actions'];
  selectedEmployee?: IEmployee;

  constructor(
    private dialog: MatDialog,
    private employeeSelectors: EmployeeSelectors,
    private store: Store
  ) {
  }

  ngOnInit(): void {
    this.employeeSelectors.employees$
      .subscribe(e => this.employees = e);
  }

  delete(employee: IEmployee): void {
    this.dialog.open(ConfirmDialogComponent, {
      data: 'Are you sure you want to delete ' + employee.name + '?'
    })
    .afterClosed()
    .pipe(
      filter(confirmed => Boolean(confirmed)),
    )
    .subscribe(() => this.store.dispatch(employeeActions.requestEmployeeDelete({ id: employee.id })));
  }

}