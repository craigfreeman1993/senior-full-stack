import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IEmployee } from '../resources/IEmployee';
import { IEmployeeService } from './IEmployeeService';

@Injectable()
export class EmployeeService implements IEmployeeService {
    constructor(private http: HttpClient) {
    }

    delete(id: number): Observable<void> {
        return this.http.delete<void>(`employees/${id}`);
    }
    
    getEmployees(): Observable<IEmployee[]> {
        return this.http.get<IEmployee[]>("employees");
    }
}
