import { InjectionToken } from "@angular/core";
import { Observable } from "rxjs";
import { IEmployee } from "../resources/IEmployee";

export const EmployeeServiceToken = new InjectionToken<IEmployeeService>('Employee Service');

export interface IEmployeeService {
    getEmployees(): Observable<IEmployee[]>;
    delete(id: number): Observable<void>;
}
